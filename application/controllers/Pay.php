<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pay extends CI_Controller{
    // public function CreateBranch(){
    //     $payload = json_decode(file_get_contents('php://input'),true);
    //     $this->branch_model->CreateBranch($payload);
    // }

    public function FetchAllProduct(){
        echo $this->Pay_model->FetchAllProduct();
    }

    public function FetchAllProductAndBrewCount(){
        echo $this->Pay_model->FetchAllProductAndBrewCount();
    }
    
    public function FetchAllFarmerComp(){
        echo $this->Pay_model->FetchAllFarmerComp();
    }
    public function FetchAllTrack(){
        echo $this->Pay_model->FetchAllTrack();
    }

    public function FetchAllTrackByBrewId(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->Pay_model->FetchAllTrackByBrewId($payload);
    }
    
    public function VerifyCode(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->Pay_model->VerifyCode($payload);
    }

    public function InsertProductBrews(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->Pay_model->InsertProductBrews($payload);
    }
    
    public function VerifyTransaction(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->Pay_model->VerifyTransaction($payload);
    }

    public function FetchAllProductBrews(){
        echo $this->Pay_model->FetchAllProductBrews();
    }
}

?>