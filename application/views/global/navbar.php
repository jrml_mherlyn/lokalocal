<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->load->view('global/styles') ?>
    <title>Lokalocal | HOME</title>
    <link rel="icon" href="public/images/icon.png" type="image/x-icon" />
  </head>
  <body>
  <nav id="lokalocal-header" class="navbar navbar-expand-lg navbar-light">
    <a class="lokalocal-brand" href="#">Lokalocal</a>
  <button class="navbar-toggler hidden" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse show" id="lokalocal-menu">
    <div class="btn-group special" role="group" aria-label="Basic example">
      <a class="btn" href="index.php">Home <i class="fas fa-star"></i></a>
      <a class="btn" href="menu">Menu <i class="fas fa-bars"></i></a>
      <a class="btn" href="pay">Pay <i class="far fa-credit-card"></i></a>
      <a class="btn" href="#" style="color:#cdcdcd !important;" disabled>Branch <i class="fas fa-store-alt"></i></a>
    </div>
  </div>
</nav>
<div class="container-fluid" style="border-top:5px solid rgb(96, 51, 31);">
  <div class="row">
    <div class="col-xs-12 mt-2 text-center">
      <p class="ml-2 font-weight-bold" style="display:inline; font-size:35px; letter-spacing:.5; margin-left:35px !important; font-weight: 800;">LokaLocal Coffee</p><br>
      <p class="ml-2 font-weight-bold font-italic" style="display:inline; font-size:15px; letter-spacing:.5; font-weight: 600;">Makes you love</p>
      <!-- <img class="img" src="http://localhost/lokalocal/public/images/Coffee.png" style="width:20px; "> -->
      <img class="img" src="public/images/Coffee.png" style="width:15px; margin-bottom:15px;">
    </div>
  </div>
  <div class="row sticky" style="z-index:9999;">
    <div class="btn-group" role="group" aria-label="Basic example">
    <a class="btn btn-secondary">Sign in <i class="far fa-user-circle"></i></a>
  </div>
  </div>
</div>