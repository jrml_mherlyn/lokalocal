<script src="public/js/qr.js"></script>
<script>
  function openQRCamera(node) {
    
    var reader = new FileReader();
    reader.onload = function() {
      node.value = "";
      qrcode.callback = function(res) {
        if(res instanceof Error) {
          alert("No QR code found. Please make sure the QR code is within the camera's frame and try again.");
            $(".qrcode-text-btn").show()
            $("#loader").fadeOut("1000")
        } else {
          $('#txtQrCode').trigger('change');
          node.parentNode.previousElementSibling.value = res;
          // alert("WORKING " + res)
          $("#txtQrCode").val(res)
            var pl = {
            qr_id : res
          }
          $.ws.executePost('Pay/VerifyCode',pl).done(function(result){
            if(result.status == "SUCCESS"){
              $("#btnSubmit").prop('disabled', false);
              setTimeout(function(){
                $(".alertPurchase").slideDown("1000");
                $(".alertPurchase").removeClass("alert-danger").addClass("alert-success")
                $(".alertPurchase").text("QR IS ACTIVE")
                $(".alertPurchase").delay("2000").slideUp("1000");
                $("#loader").fadeOut("1000")
                $("#btnSubmit").removeClass('hide');
                $(".qrcode-text-btn").show()
              },1000)
            }else{
              $(".alertPurchase").slideDown("1000");
              $(".alertPurchase").removeClass("alert-success").addClass("alert-danger")
              $(".alertPurchase").text("QR code already expired, please contact your admin.")
              $(".alertPurchase").delay("2000").slideUp("1000");
              $(".qrcode-text-btn").show()
              $("#btnSubmit").addClass('hide');
              // alert("USED");
            }
            $(".qrcode-text-btn").show()
          });
          
        }
      };
      qrcode.decode(reader.result);
    };
    reader.readAsDataURL(node.files[0]);
  }
  function showQRIntro() {
    $("#loader").fadeIn("1000")
    $(".qrcode-text-btn").hide()
  return confirm("Confirm using camera to capture QR Code?");

}
</script>
<script src="public/js/main.js"></script>
<script src="public/js/custom/ws.pay.js"></script>
<script src="public/js/custom/function.js"></script>
<script> 
 $(document).ready(function(){
   $.ws.pay.loadProducts();
 })
</script>
