<link rel="stylesheet" href="public/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

<style type="text/css">
	* {
		font-family: 'Open Sans', sans-serif;
	}
	.fixed {
	  position: fixed;
	  top:0px !important; 
	  /*left:0px !important;*/
	  width: 100%; 
	  /*padding:0px 0px 0px 15px;*/
	  background: #ffffff;
	}
	.card {
		border: 0px !important;
		padding: 10px 0px 10px 0px;
		background: rgb(96, 51, 31);
		border-radius: 0px;
	}
	.card.title{
		color: #ffffff;
		font-size: 30px;
	}
	#lokalocal-header {
		background: rgb(96, 51, 31) !important;
	}
	#lokalocal-header .lokalocal-brand{
		color: #fff !important;
		font-weight: 600;
	}
	.hidden {
		display: none !important;
	}
	.btn {
		background: #ffffff !important;
		color: rgb(96, 51, 31) !important;
		outline: none !important;
		box-shadow: 0px !important;
		border: 0px !important;
		border-radius: 0px !important;
	}
	button {
		outline: 0px !important;
		box-shadow: 0px !important;
		border: 0px !important;
	}
	.btn-secondary:hover {
		background: #ffffff !important;
		color: rgb(96, 51, 31) !important;
		outline: 0px !important;
		box-shadow: 0px !important;
		border: 0px !important;
	}
	.btn-group.special {
	  display: flex;
	}
	.special .btn {
	  flex: 1;
	}
	.opacity{
		opacity: 0;
	}
	@media (min-width: 276px) {
		#lokalocal-header .lokalocal-brand{
			display: none !important;
		}
		#lokalocal-header{
			position: fixed;
			width: 100%;
			bottom: 0px;
			z-index:9998;
		}
		.navbar {
			padding: 0px !important;
		}
		button.btn-secondary
		{
			border-radius: 0px;
			background: #ffffff;
			color: rgb(96, 51, 31);
			font-weight: 600;
			border: 0px !important;
			outline: 0px !important;
			box-shadow: 0px !important;
		}
	}

	@media (min-width: 376px) {
		#lokalocal-header .lokalocal-brand{
			display: none !important;
		}
		#lokalocal-header{
			position: fixed;
			width: 100%;
			bottom: 0px;
		}
		.navbar {
			padding: 0px !important;
		}
		button.btn-secondary
		{
			border-radius: 0px;
			background: #ffffff;
			color: rgb(96, 51, 31);
			font-weight: 600;
			border: 0px !important;
			outline: 0px !important;
			box-shadow: 0px !important;
		}
	}

	@media (min-width: 576px) {
		#lokalocal-header .lokalocal-brand{
			display: none !important;
		}
	}

	@media (min-width: 768px) {
		#lokalocal-header{
			position: relative;
			/*width: 100%;
			bottom: 0px;*/
		}
		#lokalocal-header .lokalocal-brand{
			display: none !important;
		}
	}

	@media (min-width: 992px) {
		#lokalocal-menu {
			display: none !important;
		}
		#lokalocal-header .lokalocal-brand{
			display: block !important;
		}
	}

	@media (min-width: 1200px) {
		#lokalocal-menu {
			display: none !important;
		}
		#lokalocal-header .lokalocal-brand{
			display: block !important;
		}
	}
</style>