<?php $this->load->view('global/navbar') ?>
<style>
.btn-success{
  color: #fff!important;
    background-color: #28a745!important;
    border-color: #28a745!important;
}
.hide{
  visibility: hidden
}
.qrcode-text-btn {
  margin-top: 0rem;
  width: 4em;
  height: 4em;
  background: url('public/img1/qr_icon.svg') 50% 50% no-repeat;
  cursor: pointer;
  vertical-align: middle
}

.qrcode-text-btn > input[type=file] {
  position: absolute;
  overflow: hidden;
  width: 1px;
  height: 1px;
  opacity: 0;
}

</style>
<div class="card title text-center">
  Pay Product
</div>
<div class="container-fluid">
  <div class="row mb-4" style="margin-top:10px">
    <div class="col text-center">
      <div class="alert alert-success alertPurchase" role="alert">
      </div><br>
      <label>Select Product to Purchase</label><br>
      <select class="form-control" id="selectProductPurchase" style="width:60%; margin:0 auto;" >
        <option value="0">- Select a Product -</option>
      </select>
      <br>
      <label>Select Grams</label><br>
      <select class="form-control" id="selectBrewWeight" style="width:60%; margin:0 auto;" >
        <option value="0">- Select Size -</option>
        <option value="0.04">6oz</option>
        <option value="0.06">8oz</option>
        <option value="0.08">10oz</option>
      </select>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-md-12 text-center">
      <label>Capture your QR Code</label><br>
      <input type=text id="txtQrCode" class="mt-1 qrcode-text form-control"  style="width:60%; margin:0 auto;"><br>
      <label class=qrcode-text-btn>
      <input type=file accept="image/*"  capture=environment onclick="return showQRIntro();" onchange="openQRCamera(this);" tabindex=-1 >
      </label>
      <button class="btn text-success btn-block btn-sm btn-success mt-3 hide" disabled id="btnSubmit" style="width:50%; margin:0 auto;">SUBMIT</button><img id="loader" style="position:absolute; width:50px; top:80px; left:155px;" src="public/images/loader.gif">
    </div>
  </div>
</div>
<?php $this->load->view('global/footerLau') ?>
<?php $this->load->view('global/footerPay') ?>
<script type="text/javascript">
  $(".container-fluid:nth-of-type(1)").remove();
  // $("#lokalocal-header").remove();
</script>
