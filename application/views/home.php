<?php $this->load->view('global/navbar') ?>
<div class="modal fade" id="aboutLokaLocal" tabindex="-1" role="dialog" aria-labelledby="aboutLokaLocal" aria-hidden="true" style="margin-top:0rem;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">About LokaLocal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p class="text-justify">LOFT is all about re-imagined spaces that elevate the whole working experience for freelancers, digital nomads, startups and entrepreneurs. Whether you need an office just a few times a week or for every day of the year, they have flexible and customizable plans that will fit your needs.</p>
      	<br>
      	<br>
      	<p class="text-justify">LOFT is not just an office space provider --- they’re also a business incubator for startups. They have partnered with one of the best legal and accounting firms in the metro that allows us to handle the grunt work of launching a business. View a list of our business services here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row">
		<div style="width:100%; height:150px;">
			<img class="img img-fluid" src="public/images/banner1.png">
		</div>
	</div>
</div>
<div class="container-fluid mt-3">
	<div class="row">
		<div style="width: 100%; height:130px;">
			<a class="btn btn-primary float-left btn-sm" style="background: rgb(96, 51, 31) !important; color: #ffffff !important; margin-left:20px; margin-top:100px; border-radius:20px !important; font-size:10px; padding:2px 20px 2px 20px;" data-toggle="modal" data-target="#aboutLokaLocal">Read more ...</a>
			<p style="position: absolute; width:250px; margin-top:35px; left:20px; font-size:12px;">LOFT is all about re-imagined spaces that elevate the whole working experience for </p>
			<img class="img float-right mt-3 mr-4" src="public/images/Lokaloca.png" style="width: 80px; left:240px;">
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div style="width:100%; height:150px;">
			<img class="img img-fluid" src="public/images/banner2.png">
		</div>
	</div>
</div>
<!-- <div class="container-fluid">
	<div class="row">
		<div style="width:100%; height:150px;">
			<img class="img img-fluid" src="public/images/banner1.png">
		</div>
	</div>
</div> -->
<div style="width:100%; padding-top:80px;"></div>
<?php $this->load->view('global/footer') ?>