<?php $this->load->view('global/navbar') ?>
<style type="text/css">
	.list-group-item{
		border-radius: 0px !important;
	}
	#lokalocalmenu{
		position: fixed;
		top:0px;
		width:100%;
		z-index:9998;
		height:100%;
		background:white;
	}
</style>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:5rem;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">List of Partners</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="listofPartner">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:5rem;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">List of Brews</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="listofBrews">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:5rem;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Coffee Tracking</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="listofTransactions">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="lokalocalmenu">
	<ul class="list-group">
	  <li class="list-group-item"><a href="admin">Dashboard</a></li>
	  <li class="list-group-item"><a href="brew">View Partners & Coffee Brews</a></li>
	</ul>	
</div>
<div class="container-fluid text-center" style="background:rgb(187, 125, 65); position:fixed; bottom:0px; z-index:9999;">
	<!-- <a href="index.php"><i class="far fa-dot-circle fa-3x text-white" style="padding:10px;"></i></a> -->
	<button id="openmenu" style="background:none !important;"><i class="far fa-dot-circle fa-2x text-white" style="padding:10px;"></i></button>
</div>
<div class="container-fluid" style="background:rgb(187, 125, 65); box-shadow:0px 0px 10px 1px rgb(96, 51, 31); position:fixed; top:0px; z-index:9997;">
	<p class="text-white text-center" style="padding:10px; margin:0px;">DASHBOARD</p>
</div>
<div class="container-fluid mt-5">
	<div class="row">
		<div class="col">	
			<div class="card" style="background:none;"  data-toggle="modal" data-target="#exampleModal">
			 	<img class="img" src="public/images/mug4.png" style="margin:0 auto; width:100px;">
			 	<p class="countPartners text-center small"></p>
			</div>
		</div>
		<div class="col">
			<div class="card" style="background:none;"  data-toggle="modal" data-target="#exampleModal1">
			  	<img class="img" src="public/images/mug1.png" style="margin:0 auto; width:100px;">
			  	<p class="countProducts text-center small"></p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col">	
			<div class="card" style="background:none;" data-toggle="modal" data-target="#exampleModal2">
			 	<img class="img" src="public/images/mug2.png" style="margin:0 auto; width:100px;">
			 	<p class="text-center small">(Coffee Tracking)</p>
			</div>
		</div>
		<div class="col" style="opacity:0;">
			<div class="card" style="background:none;" hidden>
			  	<img class="img" src="public/images/mug1.png" style="margin:0 auto; width:100px;">
			  	<p class=" text-center small"></p>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('global/footer') ?>
<script src="public/js/custom/function.js"></script>
<script type="text/javascript">
	$("#lokalocalmenu").slideUp("1000")
	$("#lokalocal-header").hide()
	$(".container-fluid:nth-of-type(1)").hide()
	$("#openmenu").on("click",function() {
	    $("#lokalocalmenu").slideToggle("1000");
	});
</script>