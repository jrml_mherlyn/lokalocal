<?php $this->load->view('global/navbar') ?>
<style type="text/css">
	.list-group-item{
		border-radius: 0px !important;
	}
	#lokalocalmenu{
		position: fixed;
		top:0px;
		width:100%;
		z-index:9998;
		height:100%;
		background:white;
	}
	.customBtn {
		background: #514b45 !important;
    	color: white !important;
    	padding-top:2rem;
    	padding-bottom:2rem;
	}
</style>
<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:5rem;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create New Brew</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="brewForm" hidden>
      		<div class="alert alert-success" role="alert">
			  Record Successfully Created
			</div>
      	</div>
	        <select class="form-control" id="lokalocalPartners"></select><br>
	        <input class="form-control" id="lokalocalBrewName" type="text" placeholder="Input Brew Name"><br>
	        <input class="form-control" id="lokalocalBrewPrice" type="text" placeholder="Input Brew Price">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btnAddBrew">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="fetchFarmerComp" tabindex="-1" role="dialog" aria-labelledby="fetchFarmerComp" aria-hidden="true" style="margin-top:5rem;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fetchFarmerCompLabel">Total Farmer Compensation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<h1 class="text-center" id="totalFarmerComp"></h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="lokalocalmenu">
	<ul class="list-group">
	  <li class="list-group-item"><a href="admin">Dashboard</a></li>
	  <li class="list-group-item"><a href="brew">View Partners & Coffee Brews</a></li>
	</ul>	
</div>
<div class="container-fluid text-center" style="background:rgb(187, 125, 65); position:fixed; bottom:0px; z-index:9999;">
	<!-- <a href="index.php"><i class="far fa-dot-circle fa-3x text-white" style="padding:10px;"></i></a> -->
	<button id="openmenu" style="background:none !important;"><i class="far fa-dot-circle fa-2x text-white" style="padding:10px;"></i></button>
</div>
<div class="container-fluid" style="background:rgb(187, 125, 65); box-shadow:0px 0px 10px 1px rgb(96, 51, 31); position:fixed; top:0px; z-index:9997;">
	<p class="text-white text-center" style="padding:10px; margin:0px;">Partners & Coffee Brews</p>
</div>
<div class="container-fluid" style="margin-top:5rem;">
	<div class="row" style="margin-top:1.5rem; padding:0px !important;">
		<div class="col">
			<a href="#" class="btn customBtn btn-block"   data-toggle="modal" data-target="#fetchFarmerComp">View Farmer Comission</a>
		</div>
	</div>
	<div class="row" style="margin-top:1.5rem; padding:0px !important;">
		<div class="col">
			<a href="#" class="btn customBtn btn-block openModal" data-toggle="modal" data-target="#exampleModal">Create New Brew</a>
		</div>
	</div>
</div>
<?php $this->load->view('global/footer') ?>
<script src="public/js/custom/function.js"></script>
<script type="text/javascript">
	$("#lokalocalmenu").slideUp("1000")
	$("#lokalocal-header").hide()
	$(".container-fluid:nth-of-type(1)").hide()
	$("#openmenu").on("click",function() {
	    $("#lokalocalmenu").slideToggle("1000");
	});
</script>