<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class Pay_model extends CI_Model{
    public function FetchAllProduct(){
        $get = $this->db->get('product');

        if($get->num_rows() > 0){
            $response = array(
                'status' => 'SUCCESS',
                'message' => 'SUCCESS FETCHING DATA',
                'payload' => $get->result()
            );
            echo json_encode($response);
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }

    public function FetchAllProductBrews(){
        $this->db->select('*');
        $this->db->from('product p');
        $this->db->join('product_brew pb', 'p.product_id = pb.product_id');
        $get = $this->db->get();

        if($get->num_rows() > 0){
          foreach($get->result() as $row){
            $response = array(
              'status' => 'SUCCESS',
              'message' => 'SUCCESS FETCHING DATA',
              'payload' => $get->result()
            );
          }
          echo json_encode($response);
           
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }

    public function FetchAllProductAndBrewCount(){
        $get = $this->db->query("SELECT (select COUNT(`product_id`) from `product`) AS product_count , (select count(`product_brew_id`) from `product_brew`) AS product_brew_count");
        

        if($get->num_rows() > 0){
          foreach($get->result() as $row){
            $response = array(
              'status' => 'SUCCESS',
              'message' => 'SUCCESS FETCHING DATA',
              'payload' => $get->result()
            );
          }
          echo json_encode($response);
           
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }
    
    public function FetchAllFarmerComp(){
        $get = $this->db->query("SELECT SUM(percentage) as sum FROM transaction_qr_product");
        

        if($get->num_rows() > 0){
          foreach($get->result() as $row){
            $response = array(
              'status' => 'SUCCESS',
              'message' => 'SUCCESS FETCHING DATA',
              'payload' => $get->result()
            );
          }
          echo json_encode($response);
           
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }
    public function FetchAllTrack(){
        $get = $this->db->query("SELECT product_brew_id, SUM(track_total) as sum, (SELECT product_brew_name FROM product_brew WHERE product_brew.product_brew_id = track.product_brew_id ) as product_brew_name FROM track GROUP BY product_brew_id");
        

        if($get->num_rows() > 0){
          foreach($get->result() as $row){
            $response = array(
              'status' => 'SUCCESS',
              'message' => 'SUCCESS FETCHING DATA',
              'payload' => $get->result()
            );
          }
          echo json_encode($response);
           
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }

    public function FetchAllTrackByBrewId($payload){
        $get = $this->db->query("SELECT product_brew_id, SUM(track_total) as sum, (SELECT product_brew_name FROM product_brew WHERE product_brew.product_brew_id = track.product_brew_id ) as product_brew_name FROM track WHERE product_brew_id=".$payload["product_brew_id"]." ");
        

        if($get->num_rows() > 0){
          foreach($get->result() as $row){
            $response = array(
              'status' => 'SUCCESS',
              'message' => 'SUCCESS FETCHING DATA',
              'payload' => $get->result()
            );
          }
          echo json_encode($response);
           
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }

    public function InsertProductBrews($payload){
          if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $data = array(
                'product_brew_name' => $payload["product_brew_name"],
                'product_brew_price' => $payload["product_brew_price"],
                'product_id' => $payload["product_id"]
            );
            $this->db->insert('product_brew', $data);
            $response = array(
                'status' => 'SUCCESS',
                'message' => 'SUCCESS INSERTING DATA'
            );
            echo json_encode($response);
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
        }
    }

    public function VerifyCode($payload){
          if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db->where('qr_id', $payload['qr_id']);
            $get = $this->db->get('qr');

            if($get->num_rows() > 0){
              $dt = $get->row();
              if($dt->status_flag == "A"){
                foreach($get->result() as $row){
                  $data[] = array(
                      'qrId' => $row->qr_id,
                      'qrCode' => $row->qr_code,
                      'qrAmount' => $row->qr_amount,
                      'statusFlag' => $row->status_flag,
                  );
              }
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS FETCHING DATA',
                    'payload' => $data
                );
                echo json_encode($response);
              }          
              elseif($dt->status_flag == "U"){
                $response = array(
                  'status' => 'FAILED',
                  'message' => 'QR CODE ALREADY USED'
              );
              echo json_encode($response);
              }
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }

        public function VerifyTransaction($payload){
          if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            //Fetching product brew
            $this->db->select('*');
            $this->db->from('product_brew pw');
            $this->db->where('pw.product_brew_id', $payload['product_brew_id']);
            $get = $this->db->get();
            $prod_price = $get->row()->product_brew_price;

             //Fetching qr
            $this->db->select('*');
            $this->db->from('qr q');
            $this->db->where('q.qr_id', $payload['qr_id']);
            $gett = $this->db->get();
            $stat = $gett->row()->status_flag;
            $qr_price = $gett->row()->qr_amount;
            $rem = $qr_price - $prod_price;
              if($stat == "A" ){
                $pl = array(
                  'qr_id' => $payload['qr_id'],
                  'product_brew_id' => $payload['product_brew_id'],
                  );
                  if($qr_price >= $prod_price ){                    
                    //Insert into transaction table
                    $percentage = $prod_price * .05;
                    $pl = array(
                      'qr_id' => $payload['qr_id'],
                      'product_brew_id' => $payload['product_brew_id'],
                      'transaction_amount' => $prod_price,
                      'percentage' => $percentage
                      );

                    $this->db->insert('transaction_qr_product', $pl);
                    $response = array(
                        'status' => 'SUCCESS',
                        'message' => 'SUCCESS INSERTING DATA',
                        'payload' => array(
                          'qr_amount' => $qr_price,
                          'product_amount' => $prod_price,
                          'remaining' => $rem
                        )
                      );
                      echo json_encode($response);

                    //Insert into track table  
                    $m = array(
                    "qr_amount" => $rem
                    );
                    $this->db->where('qr_id', $payload['qr_id']);
                    $this->db->update('qr', $m);
                    
                    $total = array(
                    "track_total" => $payload['serving'],
                    "product_brew_id" => $payload['product_brew_id']
                    );
                    $this->db->insert('track', $total);


                  } elseif($qr_price < $prod_price) {
                    if($qr_price == 0){
                      $d = array(
                        "status_flag" => "U"
                      );
                      $this->db->where('qr_id', $payload['qr_id']);
                      $this->db->update('qr', $d);
                      $response = array(
                        'status' => 'DEACTIVATED',
                        'message' => 'QR IS EXPENDED'
                        );
                      echo json_encode($response);
                    }else{
                      $response = array(
                        'status' => 'FAILED',
                        'message' => 'QR AMOUNT IS LESS THAN PRODUCT PRICE',
                        'payload' => array(
                          'qr_amount' => $qr_price,
                          'product_amount' => $prod_price
                        )
                        );
                      echo json_encode($response);
                    }
                   
              
                  }
                }
                elseif($stat == "U"){
                $response = array(
                  'status' => "UNAVAILABLE",
                  'message' => 'QR IS UNAVAILABLE '
              );
              echo json_encode($response);
              }
              
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }
}

?>