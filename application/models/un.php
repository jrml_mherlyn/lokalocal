public function FetchAllEmployee(){
        $this->db->select('*');
        $this->db->from('employee e');
        $this->db->join('position p', 'p.position_id = e.position_id');
        $this->db->join('user_accounts ua', 'ua.employee_id = e. employee_id');
        $this->db->join('employee_info ei', 'ei.employee_id = e.employee_id');
        $this->db->join('employee_type et', 'et.employee_type_id = e.employee_type_id');
        $this->db->join('employee_location el', 'el.employee_id = e.employee_id');
        $this->db->join('reg_psgc_region rpr', 'rpr.psgc_reg_code = el.psgc_reg_code');
        $this->db->join('reg_psgc_province rpp', 'rpp.psgc_prov_code = el.psgc_prov_code');
        $this->db->join('reg_psgc_municipality rpm', 'rpm.psgc_mun_code = el.psgc_mun_code');
        $this->db->join('reg_psgc_barangay rpb', 'rpb.psgc_brgy_code = el.psgc_brgy_code');
        $this->db->join('reg_civil_status_code rcsc', 'rcsc.civil_status_code = ei.employee_civil_status');
        $get = $this->db->get();

        if($get->num_rows() > 0){
            foreach($get->result() as $row){
                $data[] = array(
                    'employeeId' => $row->employee_id,
                    'lastName' => $row->employee_lastname,
                    'firstName' => $row->employee_firstname,
                    'middleName' => $row->employee_middlename,
                    'suffix' => $row->employee_suffix,
                    'positionTitle' => $row->position_name,
                    'employmentStatus' => $row->employee_type_name,
                    'dateHired' => $row->employee_start_date,
                    'age' => $row->employee_age,
                    'birthDate' => $row->employee_info_birthdate,
                    'civilStatus' => $row->civil_status_desc,
                    'gender' => $row->employee_info_gender,
                    'mobileNo' => $row->mobile_no,
                    'email' => $row->USER_EMAIL,
                    'employeeLocation' => $row->psgc_mun_desc
                );
            }
            $response = array(
                'status' => 'SUCCESS',
                'message' => 'SUCCESS FETCHING DATA',
                'payload' => $data
            );
            echo json_encode($response);
        }
        else{
            $response = array(
                'status'=>'FAILED',
                'message'=>'NO DATA FOUND'
            );
            echo json_encode($response);
        }
    }