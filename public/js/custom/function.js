$("#loader").fadeOut("0")
var fetch = {
    purchaseProduct: function(){
        $.ajax({
            method: "GET",
            url: "Pay/FetchAllProductBrews",
            dataType: "json",
        }).done(function (result, textStatus, jqXHR) {
            console.log(JSON.stringify(result))
            for(i=0;i<result.payload.length;i++){
                $("#selectProductPurchase").append("<option value='"+result.payload[i].product_brew_id+"'>"+result.payload[i].product_name+" -"+ result.payload[i].product_brew_name +" - "+result.payload[i].product_brew_price+"<small>php</small></option>");
            }
            for(a=0;a<result.payload.length;a++){
                $("#displayBrewProducts").append('<div class="card" style="background:none !important;">\
                                              <div class="card-body">\
                                                <img class="img round" src="public/images/mug3.png" style="margin-left:20px; width:15%;">\
                                                <div class="float-right small" style="width:150px;">\
                                                Name: '+result.payload[a].product_brew_name+'<br>\
                                                Price: '+result.payload[a].product_brew_price+'</div>\
                                              </div>\
                                            </div>')
            }
            for(z=0;z<result.payload.length;z++){
                    $("#listofBrews").append('<div class="card" style="background:none !important;">\
                                              <div class="card-body">\
                                                <img class="img round" src="public/images/mug3.png" style="margin-left:20px; width:15%;">\
                                                <div class="float-right small" style="width:150px;">\
                                                Name: '+result.payload[z].product_brew_name+'</div>\
                                              </div>\
                                            </div>')
            }
        }).fail(function (jqXHR, textStatus, errorThrown,request) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    },
    allProducts: function(){
        setTimeout(function(){
            $.ajax({
            method: "GET",
            url: "Pay/FetchAllProductAndBrewCount",
            dataType: "json",
            }).done(function (result, textStatus, jqXHR) {
                    console.log(result)
                    console.log(result.payload[0].product_count)
                    console.log(result.payload[0].product_brew_count)
                    $(".countPartners").text("("+result.payload[0].product_count+" no. of Partners)")
                    $(".countProducts").text("("+result.payload[0].product_brew_count+" no. of Brews)")
            }).fail(function (jqXHR, textStatus, errorThrown,request) {
            });
        },300)
    },
    allProducts1: function(){
        $.ajax({
            method: "GET",
            url: "Pay/FetchAllProduct",
            dataType: "json",
        }).done(function (result, textStatus, jqXHR) {
            console.log(result)
            for(a=0;a<result.payload.length;a++){
                $(".countProducts").html("("+result.payload[0].product_brew_count+" no. of Brews)")
            }
            for(b=0;b<result.payload.length;b++){
                $("#lokalocalPartners").append("<option value='"+result.payload[b].product_id+"'>"+result.payload[b].product_name+"</option>")
            }
            for(z=0;z<result.payload.length;z++){
                    $("#listofPartner").append('<div class="card" style="background:none !important;">\
                                              <div class="card-body">\
                                                <img class="img round" src="public/images/mug3.png" style="margin-left:20px; width:15%;">\
                                                <div class="float-right small" style="width:150px;">\
                                                Name: '+result.payload[z].product_name+'</div>\
                                              </div>\
                                            </div>')
            }
        }).fail(function (jqXHR, textStatus, errorThrown,request) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    },
    allTrack: function(){
        $.ajax({
            method: "GET",
            url: "Pay/FetchAllTrack",
            dataType: "json",
        }).done(function (result, textStatus, jqXHR) {
            // $(".countPartners").html("("+result.payload[0].payload_count+" no. of Kg)")
            // $(".countBrewKg").html("("+result.payload[0].sum+"Kg consumed)")
            // for(i=0;i<result.payload.length;i++){
            //     $("#selectProductPurchase").append("<option value='"+result.payload[i].product_id+"'>"+result.payload[i].product_name+" -"+ result.payload[i].product_brew_name +" - "+result.payload[i].product_brew_price+"<small>php</small></option>");
            // }
        }).fail(function (jqXHR, textStatus, errorThrown,request) {
            // console.log(jqXHR);
            // console.log(textStatus);
            // console.log(errorThrown);
        });
    },
    farmerComp: function(){
        $.ajax({
            method: "GET",
            url: "Pay/FetchAllFarmerComp",
            dataType: "json",
        }).done(function (result, textStatus, jqXHR) {
            if(result.payload.length>0){
                $("#totalFarmerComp").html("0")
                $("#totalFarmerComp").html(result.payload[0].sum+"php")
            }else{
                $("#totalFarmerComp").html(result.payload[0].sum+"php")
                
            }
        }).fail(function (jqXHR, textStatus, errorThrown,request) {
        });
    },
    transaction: function(){
        $.ajax({
            method: "GET",
            url: "Pay/FetchAllTrack",
            dataType: "json",
        }).done(function (result, textStatus, jqXHR) {
            for(a=0;a<result.payload.length;a++){
                $("#listofTransactions").append('<div class="card" style="background:none !important;">\
                                              <div class="card-body">\
                                                <img class="img round" src="public/images/mug3.png" style="margin-left:20px; width:15%;">\
                                                <div class="float-right small" style="width:150px;">\
                                                Name: Name: '+result.payload[a].product_brew_name+'<br>\
                                                Sum: '+result.payload[a].sum+'kg</div>\
                                              </div>\
                                            </div>')
            }
        }).fail(function (jqXHR, textStatus, errorThrown,request) {
        });
    }
}

var create = {
    purchaseProduct: function(value1, value2 ,value3){
        var payload = {
            "product_brew_id" : value1,
            "qr_id" : value2,
            "serving" : value3
        }
        $.ajax({
            method: "POST",
            url: "Pay/VerifyTransaction",
            dataType: "json",
            data: JSON.stringify(payload)
        }).done(function (result, textStatus, jqXHR) {
            if(result.message === "SUCCESS INSERTING DATA"){
                $(".alertPurchase").removeClass("alert-danger").addClass("alert-success")
                $(".alertPurchase").html(result.message)
                $("#loader").fadeOut("1000")   
                $(".alertPurchase").delay("3000").slideUp("1000")
                setTimeout(function(){
                    location.reload();
                },3000)
            }else{
                $(".alert-success").addClass("alert-danger").removeClass("alert-success")
                $(".alert-danger").slideDown("1000")
                $(".alert-danger").html(result.message)
                $(".alert-danger").delay("3000").slideUp("1000")
            }

        }).fail(function (result, textStatus, errorThrown,request) {
        });
    },
    newProductBrew: function(value1, value2, value3){
        var payload = {
            "product_id": value1,
            "product_brew_name" : value2,
            "product_brew_price":  value3
        }
        $.ajax({
            method: "POST",
            url: "Pay/InsertProductBrews",
            dataType: "json",
            data: JSON.stringify(payload)
        }).done(function (result, textStatus, jqXHR) {
                $("#brewForm").removeAttr("hidden")
            setTimeout(function(){
                $("#brewForm").attr("hidden","hidden");
                $("#lokalocalBrewName").val("");
                $("#lokalocalBrewPrice").val("");
                $("#exampleModal").modal('toggle');
            },3000)
        }).fail(function (jqXHR, textStatus, errorThrown,request){
        });
    }
}
$(".alertPurchase").slideUp("0")
$("#btnSubmit").on("click", function(){
    var product_purchased = $("#selectProductPurchase option:selected").val();
    var grams = $("#selectBrewWeight option:selected").val();
    var qr_code = $("#txtQrCode").val();
    create.purchaseProduct(product_purchased,qr_code,grams)
});
$(".btnAddBrew").on("click", function(){
    var selected_partner = $("#lokalocalPartners option:selected").val();
    var brew_name = $("#lokalocalBrewName").val();
    var brew_price = $("#lokalocalBrewPrice").val();
    create.newProductBrew(selected_partner,brew_name,brew_price)
});

fetch.farmerComp();
fetch.transaction();
fetch.allProducts();
fetch.purchaseProduct();
fetch.allProducts1();