-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 18, 2018 at 07:57 AM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loca`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`) VALUES
(1, 'Local Arie\'s'),
(2, 'Local Lau\'s'),
(3, 'Local Lim\'s'),
(4, 'Local Schub\'s'),
(5, 'Local Elin\'s');

-- --------------------------------------------------------

--
-- Table structure for table `product_brew`
--

CREATE TABLE `product_brew` (
  `product_brew_id` int(11) NOT NULL,
  `product_brew_name` text NOT NULL,
  `product_brew_price` float(20,2) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_brew`
--

INSERT INTO `product_brew` (`product_brew_id`, `product_brew_name`, `product_brew_price`, `product_id`) VALUES
(1, 'Alisa- Dark Brew', 100.00, 1),
(2, 'Alisa-Light Brew', 50.00, 1),
(3, 'Lau- Light Brew with Cream', 90.00, 2),
(4, 'Lau -Dark Brew with Cream', 130.00, 2),
(5, 'Lim- Medium Brew', 30.00, 3),
(6, 'Schubs- Heavy Brew', 60.00, 4),
(7, 'Elin- Heavy Brew with Cream', 180.00, 5),
(8, 'Schubs- Cream Latte', 50.00, 4);

-- --------------------------------------------------------

--
-- Table structure for table `qr`
--

CREATE TABLE `qr` (
  `qr_id` int(11) NOT NULL,
  `qr_code` text NOT NULL,
  `qr_amount` float(20,2) DEFAULT NULL,
  `status_flag` text NOT NULL,
  `qr_deact_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qr`
--

INSERT INTO `qr` (`qr_id`, `qr_code`, `qr_amount`, `status_flag`, `qr_deact_date`) VALUES
(1, '0001', 500.00, 'A', NULL),
(2, '0002', 200.00, 'A', NULL),
(3, '0003', 400.00, 'A', NULL),
(4, '0004', 250.00, 'U', NULL),
(5, '0005', 300.00, 'A', NULL),
(6, '0006', 210.00, 'A', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `track`
--

CREATE TABLE `track` (
  `track_id` int(11) NOT NULL,
  `product_brew_id` int(11) NOT NULL,
  `track_total` float(20,2) DEFAULT NULL,
  `track_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_qr_product`
--

CREATE TABLE `transaction_qr_product` (
  `transaction_id` int(11) NOT NULL,
  `qr_id` int(11) NOT NULL,
  `product_brew_id` int(11) NOT NULL,
  `transaction_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `transaction_amount` float(20,2) NOT NULL,
  `percentage` float(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_brew`
--
ALTER TABLE `product_brew`
  ADD PRIMARY KEY (`product_brew_id`);

--
-- Indexes for table `qr`
--
ALTER TABLE `qr`
  ADD PRIMARY KEY (`qr_id`);

--
-- Indexes for table `track`
--
ALTER TABLE `track`
  ADD PRIMARY KEY (`track_id`);

--
-- Indexes for table `transaction_qr_product`
--
ALTER TABLE `transaction_qr_product`
  ADD PRIMARY KEY (`transaction_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_brew`
--
ALTER TABLE `product_brew`
  MODIFY `product_brew_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `qr`
--
ALTER TABLE `qr`
  MODIFY `qr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `track`
--
ALTER TABLE `track`
  MODIFY `track_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transaction_qr_product`
--
ALTER TABLE `transaction_qr_product`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
